Ensemble de la documentation et des livrables de la mission d'accompangement CDG59-Somme numérique : 
 * documentation technique
 * documentation organisationnelle
 
 * dossier d'exploitation du SAE
    * politique d'archivage
    * MOO
    * RSSI
    * contrats de service
    * PRI/PCI

* référentiel des besoins

* grille d'audit
* analyse des vulnérabilités
